
class Service {
  String id;
  List<dynamic> image;
  String name;
  int price;
//  int discount;
  String description;
//  int quantity;
//  String amount;
//  List<ProductVariety> variety;

  Service({ this.price, this.image, this.name, this.id,  this.description});

  Service.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    image = json['image'];
    name = json['name'];
    price = json['price'];
//    quantity = json['quantity'];
    description = json['description'];
//    discount = json['discount'];
//    amount = json['amount'];
//    if (json['variety'] != null) {
//      variety = new List<ProductVariety>();
//      json['variety'].forEach((v) {
//        variety.add(new ProductVariety.fromJson(v));
//      });
//    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['image'] = this.image;
    data['name'] = this.name;
    data['price'] = this.price;
//    data['quantity'] = this.quantity;
//    data['discount'] = this.discount;
    data['description'] = this.description;
//    data['amount'] = this.amount;
//    if (this.variety != null) {
//      data['variety'] = this.variety.map((v) => v.toJson()).toList();
//    }
    return data;
  }

  static List<Service> services = List();
//
//  static final List<Product> menu = [
//
//    Product(
//        price: 650,
//        name: "Taglio",
//        id:"A",
//        description: "Coffe beans responsibly sourced in 500g package",
////        discount: 0,
//        image: [
//          "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/coffebeans.jpg?alt=media&token=50e40816-8bf1-4788-a64d-501fdbf7e2f2",
//          "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/IMG_7917.jpg?alt=media&token=50e40026-ea1c-44a0-b3da-0f536ee99863",
//          "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/IMG_7945.jpg?alt=media&token=08ebe792-0565-4185-af70-4b28a8bb7053"
//        ],
//
//        variety: [
//          ProductVariety("A1", "A",
//              "Brazil",
//              "Natural process, medium acidity, full body.",
//              650 ,
//              "Flavours of chocolate and nuts with soft acidity.",
//              "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/brazil.jpg?alt=media&token=bdd11fa7-b276-4687-8725-d0ff4c73ae2e"),
//          ProductVariety("A2","A",
//              "Colombia",
//              "Honey, sun dried, medium acidity, full body.",
//              630 ,
//              "Malt and berries in the fragrance. In the cup we find chicolate spices, franberry and tropical florals.",
//              "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/colombia.jpg?alt=media&token=01d0e80e-b352-4683-8934-d897a04ec5da"),
//          ProductVariety("A3","A",
//              "Costa Rica",
//              "Arabica, washed, medium acidity, medium body.",
//              600 ,
//              "Sweet, refined cup with notes chocolate, honey and apricot. Green apple acidity.",
//              "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/costa%20rica.jpg?alt=media&token=963fb23f-37ca-43c2-a875-935f136a585a"),
//          ProductVariety("A4","A",
//              "Guatemala",
//              "Arabic, washed, medium acidity, medium body. ",
//              750 ,
//              "Sweet caramel fragrance, smooth brown sugar notes, delicate lemon acidity.",
//              "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/guatemala.jpg?alt=media&token=c745daa9-cf10-4827-ac52-d15d1b6ed35e"),
//          ProductVariety("A5","A",
//              "India Parchment",
//              "Robusta Parchment, low acidity, full body.",
//              680 ,
//              "A delicious combination of caramel, chocolate and hazelnut, with mali, caram and toasted bred fragrance.",
//              "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/india.jpg?alt=media&token=c262732e-6b48-4466-b357-dcc0c5daadb6"),
//          ProductVariety("A6","A",
//              "Mexico UTZ",
//              "Arabica, washed, low acidity, medium body.",
//              630 ,
//              "Refreshing and vibrant, soft citrus lie sweet lime.",
//              "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/mexico.jpg?alt=media&token=48d025a7-e4f8-488c-84b0-c0c96973dc79"),
//          ProductVariety("A7","A",
//              "Nicaragua",
//              "Washed, medium acidity, full body.",
//              650 ,
//              "Sharp and intense with tobacco and nut notes.",
//              "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/nicaragua.jpg?alt=media&token=fa31877b-17c0-4b77-b097-fcfa6cd65869"),
//          ProductVariety("A8","A",
//              "Peru",
//              "Washed, medium to high acidity, medium body.",
//              680 ,
//              "Fragrant with sweet chocolate and caramel notes. Citric acidity.",
//              "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/peru.jpg?alt=media&token=7a14f5b8-6360-4d78-ba03-949e34c4c98d"),
//        ]
//    ),
//    Product(
//        price: 550,
//        name: "Ground Coffee",
//        id:"B",
//        description: "Ground coffee from differenet origins, 250g or 500g package",
//        discount: 0,
//        image: [
//          "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/IMG_7888.jpg?alt=media&token=fc26e83a-2f85-4eaf-bb09-4d1297e7b7d2",
//          "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/IMG_7930.jpg?alt=media&token=ac8c79ed-e154-4ed1-86ba-5ff83b4a581f"
//        ],
//        variety: [
//          ProductVariety("B1","B",
//              "Brazil",
//              "Natural process, medium acidity, full body.",
//              650 ,
//              "Flavours of chocolate and nuts with soft acidity.",
//              "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/brazil.jpg?alt=media&token=bdd11fa7-b276-4687-8725-d0ff4c73ae2e"),
//          ProductVariety("B2","B",
//              "Colombia",
//              "Honey, sun dried, medium acidity, full body.",
//              630 ,
//              "Malt and berries in the fragrance. In the cup we find chicolate spices, franberry and tropical florals.",
//              "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/colombia.jpg?alt=media&token=01d0e80e-b352-4683-8934-d897a04ec5da"),
//          ProductVariety("B3","B",
//              "Costa Rica",
//              "Arabica, washed, medium acidity, medium body.",
//              600 ,
//              "Sweet, refined cup with notes chocolate, honey and apricot. Green apple acidity.",
//              "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/costa%20rica.jpg?alt=media&token=963fb23f-37ca-43c2-a875-935f136a585a"),
//          ProductVariety("B4","B",
//              "Guatemala",
//              "Arabic, washed, medium acidity, medium body. ",
//              750 ,
//              "Sweet caramel fragrance, smooth brown sugar notes, delicate lemon acidity.",
//              "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/guatemala.jpg?alt=media&token=c745daa9-cf10-4827-ac52-d15d1b6ed35e"),
//          ProductVariety("B5","B",
//              "India Parchment",
//              "Robusta Parchment, low acidity, full body.",
//              680 ,
//              "A delicious combination of caramel, chocolate and hazelnut, with mali, caram and toasted bred fragrance.",
//              "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/india.jpg?alt=media&token=c262732e-6b48-4466-b357-dcc0c5daadb6"),
//          ProductVariety("B6","B",
//              "Mexico UTZ",
//              "Arabica, washed, low acidity, medium body.",
//              630 ,
//              "Refreshing and vibrant, soft citrus lie sweet lime.",
//              "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/mexico.jpg?alt=media&token=48d025a7-e4f8-488c-84b0-c0c96973dc79"),
//          ProductVariety("B7","B",
//              "Nicaragua",
//              "Washed, medium acidity, full body.",
//              650 ,
//              "Sharp and intense with tobacco and nut notes.",
//              "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/nicaragua.jpg?alt=media&token=fa31877b-17c0-4b77-b097-fcfa6cd65869"),
//          ProductVariety("B8","B",
//              "Peru",
//              "Washed, medium to high acidity, medium body.",
//              680 ,
//              "Fragrant with sweet chocolate and caramel notes. Citric acidity.",
//              "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/peru.jpg?alt=media&token=7a14f5b8-6360-4d78-ba03-949e34c4c98d"),
//        ]
//    ),
//    Product(
//      price: 3500,
//      name: "Coffe Pods",
//      id:"C",
//      description: "Compostable Coffe pods Nespresso compatible.\nPack of 100 units.",
//      discount: 0,
//      image: ["https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/IMG_7900.jpg?alt=media&token=79e29ecb-ee5c-44c7-9d9f-bad110ca36db",
//        "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/IMG_7942.jpg?alt=media&token=46821165-0acb-41b5-937b-5741a4df7041"],
//    ),
//    Product(
//        price: 2500,
//        name: "Coffe Pads",
//        id:"D",
//        description: "These coffe pods are compatible with every machine that uses ground coffee.\nPack of 100 units.",
//        discount: 0,
//        image: [
//          "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/IMG_7905.jpg?alt=media&token=7122683b-37c2-462b-a3f9-e425091f4a83"
//          ,
//          "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/IMG_7910.jpg?alt=media&token=15ad5c03-049b-4292-925e-f4f48da9d796"
//          ,
//          "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/IMG_7911.jpg?alt=media&token=49427baa-f0f2-44b6-94e1-b702ec267b0b"
//          ,
//          "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/IMG_7934.jpg?alt=media&token=11c8b62c-4f18-4bab-a9e4-7bd9e671891d"
//        ]
//    ),
//    Product(
//        price: 1050,
//        name: "Coffe beans 3kg",
//        id:"E",
//        description: "Our own blend of coffee beans.",
//        discount: 0,
//        image: [
//          "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/IMG_7947-min.jpg?alt=media&token=f16ad44a-549c-48b9-8252-685a8ffb688f"
//        ]
//    ),
//    Product(
//        price: 1050,
//        name: "Coffe beans 3kg",
//        id:"F",
//        description: "Our own blend of coffee beans.",
//        discount: 0,
//        image: [
//          "https://firebasestorage.googleapis.com/v0/b/caffe-latino-6b6a5.appspot.com/o/IMG_7951-min.jpg?alt=media&token=1ad911ad-b86c-4b15-badc-a390fbe2b5fd"
//        ]
//    ),
//  ];
//
}
//
//class ProductVariety {
//  String id;
//  String parentId;
//  String name;
//  String description;
//  String details;
//  int quantity;
//  int price;
//  String amount;
//  String image;
//
//  ProductVariety(this.id, this.parentId, this.name, this.description, this.price, this.details, this.image);
//
//  ProductVariety.fromJson(Map<String, dynamic> json) {
//    id = json['id'];
//    parentId = json['parentId'];
//    name = json['name'];
//    price = json['price'];
//    quantity = json['quantity'];
//    description = json['description'];
//    details = json['details'];
//    amount = json['amount'];
//    image = json['image'];
//  }
//
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    data['id'] = this.id;
//    data['parentId'] = this.parentId;
//    data['name'] = this.name;
//    data['price'] = this.price;
//    data['quantity'] = this.quantity;
//    data['details'] = this.details;
//    data['description'] = this.description;
//    data['amount'] = this.amount;
//    data['image'] = this.image;
//    return data;
//  }
//}

class Db {
  List<Service> services;

  Db(this.services);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.services != null) {
      data['services'] = this.services.map((v) => v.toJson()).toList();
    }
    return data;
  }

  Db.fromJson(Map<String, dynamic> json) {
    if (json['services'] != null) {
      services = new List<Service>();
      json['services'].forEach((v) {
        services.add(new Service.fromJson(v));
      });
    }
  }

}