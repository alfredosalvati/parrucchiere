import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:parrucchiere/objects/services.dart';
import 'dart:async';


class FirestoreDb {
  Firestore _db = Firestore.instance;


  Future<List<Service>> getServices() async {

    List<Service> serviceList = new List();

    _db.collection("menu").document("first").collection("services").orderBy(
        'price', descending: false)
        .snapshots()
        .forEach((snapshot) {
      for (var document in snapshot.documents) {
        serviceList.add(Service.fromJson(document.data));
      }
    });

    return serviceList;

  }

  Future<List<String>> getWhoImages() async {
    List<String> images = new List();

    _db.collection("images").document("images").collection("who")
        .snapshots()
        .forEach((snapshot) {
      for (var document in snapshot.documents) {
        images.add(document.data["url"]);
      }
    });

    return images;

  }

  Future<List<String>> getMediaImages() async {
    List<String> images = new List();

    _db.collection("images").document("images").collection("media")
        .snapshots()
        .forEach((snapshot) {
      for (var document in snapshot.documents) {
        images.add(document.data["url"]);
      }
    });

    return images;
  }

  Future<List<String>> getMediaVideos() async {
    List<String> videos = new List();

    _db.collection("videos").document("videos").collection("media")
        .snapshots()
        .forEach((snapshot) {
      for (var document in snapshot.documents) {
        videos.add(document.data["url"]);
      }
    });

    return videos;
  }

  Future<List<String>>  getText() async {
    List<String> contentList = new List();

    _db.collection("text").document("text").collection("who")
        .snapshots()
        .forEach((snapshot) {
      for (var document in snapshot.documents) {
        contentList.add(document.data["description"]);
      }
    });

    return contentList;
  }


}