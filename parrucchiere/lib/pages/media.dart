
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:parrucchiere/objects/media_content.dart';
import 'package:parrucchiere/pages/video_list.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

/// Homepage
class Media extends StatefulWidget {
  @override
  _MediaState createState() => _MediaState();
}

class _MediaState extends State<Media> {

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child :Container(
                padding: EdgeInsets.only(bottom: 30, top: 20, left: 10, right: 10),
                color: Colors.white,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text("Media",  style: Theme.of(context).textTheme.headline2,),
                      SizedBox(height: 10,),
                      Text("Video", style: Theme.of(context).textTheme.headline3,),
                      Divider(height: 2, thickness: 2, color: Colors.blue, ),
                      SizedBox(height: 10,),
                      VideoList(),
                      SizedBox(height: 10,),
                      Text("Photos", style: Theme.of(context).textTheme.headline3,),
                      Divider(height: 2, thickness: 2, color: Colors.blue, ),
                      SizedBox(height: 10,),
                      buildCarousel()
                    ]
                )
            )
        )
    );
  }

  int _index =0;

  Widget buildCarousel() {
    return CarouselSlider(
        items: getImages(MediaContent.images),
        options: CarouselOptions(
          height: MediaQuery.of(context).size.height * 0.3,
          aspectRatio: 1.0,
          viewportFraction: 1.0,
          initialPage: 0,
          enableInfiniteScroll: true,
          reverse: false,
          autoPlay: false,
          autoPlayInterval: Duration(seconds: 3),
          autoPlayAnimationDuration: Duration(milliseconds: 800),
          autoPlayCurve: Curves.fastOutSlowIn,
          //pauseAutoPlayOnTouch: Duration(seconds: 10),
          enlargeCenterPage: true,
          onPageChanged: (index, mode) => {
            setState(() {
              _index = index;
            }),
          },
          scrollDirection: Axis.horizontal,
        )
    );
  }

  List<Widget> getImages(List<dynamic> strings)
  {
    return strings.map((item) => new Image.network(item, fit: BoxFit.fill,)).toList();
  }

}