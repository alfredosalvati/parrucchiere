
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:parrucchiere/common/colors.dart';
import 'package:parrucchiere/objects/services.dart';
import 'package:provider/provider.dart';



class ServiceDetail extends StatefulWidget {
  Service service;
  VoidCallback action;


  ServiceDetail(this.service, this.action);

  @override
  _ServiceDetailState createState() => _ServiceDetailState();

}

class _ServiceDetailState extends State<ServiceDetail>  {


  var bloc;
  ThemeData theme;

  double safePaddingTop;
  int _index =0;

  @override
  void initState() {
    super.initState();


  }

  @override
  void dispose() {
    super.dispose();
  }





  @override
  Widget build(BuildContext context) {
    double safePadding = MediaQuery.of(context).padding.bottom;
    safePaddingTop = MediaQuery.of(context).padding.top;

    theme = Theme.of(context);
    return Stack(
      children: [
        Container(
          color: AppColors.white,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: [
//              Expanded(
//                flex: 2,
//                child:
              buildPhoto(context),
              //),
              buildDots(),

              Flexible(
                flex: 2,
                child: Container(
                  width: double.infinity,
                  height: 350,

                  child: buildDescription(),
                ),
              ),

            ],


          ),
        ),

      ],
    );
  }


  Stack buildPhoto(BuildContext context) {
    return Stack(
      children: [

          Container(
            constraints: BoxConstraints(
                maxHeight: 400,
                minWidth: 400
            ),
            height: MediaQuery.of(context).size.width,
            child:  buildCarousel(widget.service),
            width: MediaQuery.of(context).size.width,
          ),

        Positioned(
            top: safePaddingTop,
            left: 10,
            child: Material(
              color: Colors.transparent,
              child: IconButton(
                icon:Icon( Icons.chevron_left, size: 34,),
                color: AppColors.brown,
                onPressed: () => widget.action(),
              ),
            )
        )
      ],
    );
  }

  Widget buildDescription() {
    return SingleChildScrollView(
        child: Column (
          mainAxisAlignment: MainAxisAlignment.start,

          children: [
            Padding(
              padding: EdgeInsets.only(top: 20, bottom: 10) ,
              child: Text(widget.service.name ,textAlign: TextAlign.left,style:
              theme.textTheme.headline.copyWith(color: AppColors.black)),
            ),
            Padding(
                padding: EdgeInsets.only(left: 30, right: 30, bottom: 20),
                child:  Text(widget.service.description.replaceAll("\\n", "\n"),textAlign: TextAlign.center,style:
                theme.textTheme.body2.copyWith(fontSize: 17, color: AppColors.black, fontWeight: FontWeight.w300),
                )
            ),
            Padding(
                padding: EdgeInsets.only(left: 30, right: 30, bottom: 20),
                child:Text(( NumberFormat.currency(symbol: "£", ).format(widget.service.price/100)) ,textAlign: TextAlign.center,style:
                theme.textTheme.body2.copyWith(fontSize: 17, color: AppColors.black, fontWeight: FontWeight.w300),
                )
            ),
          ]
        )
    );
  }





  Widget buildCarousel(Service service) {

    return CarouselSlider(
        items: getImages(service.image),
        options: CarouselOptions(
          height: 1000,
          aspectRatio: 1.0,
          viewportFraction: 1.0,
          initialPage: 0,
          enableInfiniteScroll: false,
          reverse: false,
          autoPlay: false,
          autoPlayInterval: Duration(seconds: 3),
          autoPlayAnimationDuration: Duration(milliseconds: 800),
          autoPlayCurve: Curves.fastOutSlowIn,
          //pauseAutoPlayOnTouch: Duration(seconds: 10),
          enlargeCenterPage: true,
          onPageChanged: (index, mode) => {
            setState(() {
              _index = index;
            }),
          },
          scrollDirection: Axis.horizontal,
        )
    );
  }

  List<Widget> getImages(List<dynamic> strings)
  {
    return strings.map((item) => new Image.network(item, fit: BoxFit.fill,)).toList();
  }


  Widget buildDots() {
    return DotsIndicator(
      dotsCount: widget.service.image.length,
      position: _index != null ? (_index).toDouble() : 0,
      decorator: DotsDecorator(
        color: AppColors.black,
        activeColor: AppColors.brown,
      ),
    );
  }


}
