
import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:map_launcher/map_launcher.dart';
import 'package:parrucchiere/util/contact_utils.dart';
import 'package:flutter/services.dart' show rootBundle;

/// Homepage
class ContactPage extends StatefulWidget {
  @override
  _ContactPageState createState() => _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {

  String _mapStyle;
  Completer<GoogleMapController> _controller = Completer();

  static const LatLng _center = const LatLng(45.521563, -122.677433);

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
    _controller.future.then((controller) => controller.setMapStyle(_mapStyle));
  }



  @override
  void initState() {
    super.initState();

    rootBundle.loadString('assets/googleMapsStyle.json').then((string) {
      _mapStyle = string;
    });
    _addMarker(_center, "Parrucchiere", "Trovami qui");
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child :Container(
                height: 900,
                padding: EdgeInsets.only(bottom: 20, top: 20, left: 20, right: 20),
                color: Colors.white,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text("Contacts",  style: Theme.of(context).textTheme.headline2,),
                      Expanded(
                          flex: 1,
                          child:Contact()
                      ),
                      Expanded(
                        flex: 2,
                        child: Container(
//                          height: 400,
                          padding: EdgeInsets.only(top: 10),
                          child: GoogleMap(
                            onMapCreated: _onMapCreated,
                            initialCameraPosition: CameraPosition(
                              target: _center,
                              zoom: 11.0,
                            ),
                            gestureRecognizers: Set()
                              ..add(Factory<PanGestureRecognizer>(() => PanGestureRecognizer()))
                              ..add(Factory<ScaleGestureRecognizer>(() => ScaleGestureRecognizer()))
                              ..add(Factory<TapGestureRecognizer>(() => TapGestureRecognizer()))
                              ..add(Factory<VerticalDragGestureRecognizer>(
                                      () => VerticalDragGestureRecognizer())),
                            myLocationEnabled: true,
                            myLocationButtonEnabled: false,
                            markers: _markers,
                          ),
                        )
                      )
                    ]
                )
            )
        )
    );
  }

  final Set<Marker> _markers = {};

  void _addMarker(LatLng location, String address, String snippet) {
    _markers.add(Marker(
        markerId: MarkerId(location.toString()),
        position: location,
        infoWindow: InfoWindow(title: address,  snippet: snippet)));
  }

}



class Contact extends StatelessWidget {
  ThemeData theme;

  @override
  Widget build(BuildContext context) {
    theme = Theme.of(context);


    return Container(
        child: ListView(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ListTile(
                  title: Text("info@parrucchiere.com",
                      style: theme.textTheme.headline3.copyWith(
                          color: Colors.black,
                          decoration: TextDecoration.underline,
                          fontWeight: FontWeight.bold)),
                  enabled: true,
                  leading: Icon(Icons.email),
                  onTap: () => ContactUtils.email("info@parrucchiere.com"),
                ),
                ListTile(
                  title: Text("Via dei parrucchieri, 24, 03012 Anagni",
                      style: theme.textTheme.headline3.copyWith(
                          color: Colors.black,
                          decoration: TextDecoration.underline,
                          fontWeight: FontWeight.bold)
                  ),
                  enabled: true,
                  leading: Icon(Icons.place),
                  onTap: () => ContactUtils.openMapsSheet(context),
                ),
                ListTile(
                  title: Text("0039077444444",
                      style: theme.textTheme.headline3.copyWith(
                          color: Colors.black,
                          decoration: TextDecoration.underline,
                          fontWeight: FontWeight.bold)),
                  enabled: true,
                  leading: Icon(Icons.computer),
                  onTap: () => ContactUtils.launchWhatsApp(phone: "0039077444444", message: "Ciao"),
                ),
                SizedBox(height: 30,),
              ],
            )
          ],
        )
    );
  }
}