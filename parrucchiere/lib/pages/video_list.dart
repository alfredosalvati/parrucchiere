import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:parrucchiere/objects/media_content.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

/// Creates list of video players
class VideoList extends StatefulWidget {
  @override
  _VideoListState createState() => _VideoListState();
}

class _VideoListState extends State<VideoList> {
  final List<YoutubePlayerController> _controllers = MediaContent.videos
      .map<YoutubePlayerController>(
        (videoId) => YoutubePlayerController(
      initialVideoId: YoutubePlayer.convertUrlToId(videoId),
      flags: const YoutubePlayerFlags(
        disableDragSeek: true,
        autoPlay: false,
      ),
    ),
  )
      .toList();

  @override
  Widget build(BuildContext context) {
    return
          SizedBox (
            height: MediaQuery.of(context).size.height * 0.3,
            width: MediaQuery.of(context).size.width,
            child: ListView.separated(
              shrinkWrap: true,
              physics: BouncingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
//              if(index % 2==0) {
                return Container(
                  padding: EdgeInsets.only(right: 10),
//                    color: Colors.red,
                    width: MediaQuery.of(context).size.width * 0.85,
//                    child: IgnorePointer(
                      child: YoutubePlayer(
                          key: ObjectKey(_controllers[index]),
                          controller: _controllers[index],
                          actionsPadding: const EdgeInsets.only(left: 16.0),
//                    )
//            bottomActions: [
//              CurrentPosition(),
//              const SizedBox(width: 10.0),
//              ProgressBar(isExpanded: true),
//              const SizedBox(width: 10.0),
//              RemainingDuration(),
//              FullScreenButton(),
//            ],
                    )
                );
              },
              itemCount: _controllers.length,
              separatorBuilder: (context, _) => const SizedBox(height: 10.0),
            ),

    );
  }

}