import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:parrucchiere/common/colors.dart';
import 'package:parrucchiere/objects/services.dart';
import 'package:parrucchiere/objects/who_content.dart';
import 'package:parrucchiere/pages/service_detail.dart';

class ServiceList extends StatelessWidget {
  final Color color;

  ServiceList(this.color);


  List<String> content = WhoContent.content;
  List<String> images = WhoContent.images;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//        backgroundColor: AppColors.white,
        body: SafeArea(
            child: SingleChildScrollView(
                child:Container (
                    color: color,
                    padding: EdgeInsets.only(bottom: 30, top: 20, left: 10, right: 10),
                    child: Column(
                      children: [
                        Text("Our Services", style: Theme.of(context).textTheme.headline2,),
                        SizedBox(height: 10,),
                        GridView.builder(
                            gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount:2, childAspectRatio: 1.2 ),
                            shrinkWrap: true,
//                          cacheExtent: 10000.0,
                            physics:BouncingScrollPhysics() ,
                            itemCount: Service.services.length,
                            itemBuilder: (context, index) {
                              return buildJobTile(Service.services[index]);
                            }
                        )
                      ],
                    )

                )
            )
        )
    );
  }

  Widget buildJobTile(Service service) {
    return Padding(
        padding: EdgeInsets.only(top: 12, left: 10, right: 10, bottom: 12),
        child: OpenContainer(
            openColor: AppColors.brown,
            closedElevation: 5.0,
            closedShape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            tappable: false,
            transitionType: ContainerTransitionType.fadeThrough,
            transitionDuration: const Duration(milliseconds:800),
            openBuilder: (context, action) {
              return  ServiceDetail(service, action);
            },
            closedBuilder: (BuildContext c, VoidCallback action) {
              return InkWell(
                      onTap: () {
                        action();
                      },
                      child: Center(
                          child: Column (
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(service.name, textAlign: TextAlign.center, style: Theme.of(c).textTheme.bodyText1,),
                              Text(NumberFormat.currency(symbol: "€", ).format(service.price/100), style: Theme.of(c).textTheme.headline5.copyWith(fontSize: 16),), //just for testing, will fill with image later
                            ],
                        ),
                      )
//                    )
              );
            }
        )
    );
  }


  FadeInImage buildFadeInImage(String imageUrl) {
    return FadeInImage(
        width: double.infinity,
        placeholder: AssetImage('assets/img/loading2.gif',),
        image: NetworkImage(imageUrl),
        fit: BoxFit.fill);
  }


}