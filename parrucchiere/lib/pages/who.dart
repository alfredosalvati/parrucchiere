import 'package:flutter/material.dart';
import 'package:parrucchiere/common/colors.dart';
import 'package:parrucchiere/objects/who_content.dart';

class Who extends StatelessWidget {
  final Color color;

  Who(this.color);


  List<String> content = WhoContent.content;
  List<String> images = WhoContent.images;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.white,
        body: SafeArea(
            child: SingleChildScrollView(

                child:Container (
                  color: color,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                          padding: EdgeInsets.only(right: 5, left: 5, bottom: 30),
                          child: Image.asset('assets/img/logo.png',
                            width: MediaQuery.of(context).size.width*0.3,
                            //height: MediaQuery.of(context).size.height*0.4,
                          )
                      ),

                      Flexible(
                        flex: 1,
                        child: buildFadeInImage(images[0]),
                      ),
                      Flexible(
                        flex: 1,
                        child: Container(
                          padding: EdgeInsets.only(right: 20, left: 20, top: 30, bottom: 30),
                          child: Text(content[0],
                              textAlign: TextAlign.center,
                              style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 20, color: Colors.black, height: 1.7)),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child:  buildFadeInImage(images[1]),
                      ),
                      Flexible(
                        flex: 1,
                        child: Container(
                          padding: EdgeInsets.only(right: 20, left: 20, top: 30, bottom: 30),
                          child: Text(content[1],
                              textAlign: TextAlign.center,
                              style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 20, color: Colors.black, height: 1.7)),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: Container(
                          padding: EdgeInsets.only(right: 20, left: 20, top: 30, bottom: 30),
                          child: Text(content[2],
                              textAlign: TextAlign.center,
                              style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 20, color: Colors.black, height: 1.7)),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child: buildFadeInImage(images[2]),

                      ),
                      Flexible(
                        flex: 1,
                        child: Container(
                          padding: EdgeInsets.only(right: 20, left: 20, top: 30, bottom: 30),
                          child: Text(content[3],
                              textAlign: TextAlign.center,
                              style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 20, color: Colors.black, height: 1.7)),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        child:  buildFadeInImage(images[3]),
                      ),
                      Flexible(
                        flex: 1,
                        child: Container(
                          padding: EdgeInsets.only(right: 20, left: 20, top: 30, bottom: 30),
                          child: Text(content[4],
                              textAlign: TextAlign.center,
                              style: Theme.of(context).textTheme.bodyText1.copyWith(fontSize: 20, color: Colors.black, height: 1.7)),
                        ),
                      ),
                    ],
                  ),
                )
            )
        )
    );
  }


  FadeInImage buildFadeInImage(String imageUrl) {
    return FadeInImage(
        width: double.infinity,
//                            height: 300,
        placeholder: AssetImage('assets/img/loading2.gif',),
        image: NetworkImage(imageUrl),
        fit: BoxFit.fill);
  }
}