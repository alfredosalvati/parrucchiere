import 'dart:ui' show Color;


class AppColors extends Object {

  // Brand Colors

  static const Color grey = const Color(0xfffafbff);
  static const Color green = const Color(0xff1e3d42);
  static const Color purple = const Color(0xff47221a);
  static const Color black = const Color(0xff000000);
  static const Color white = const Color(0xffffffff);
  static const Color orange = const Color(0xffF2A057);
  static const Color yellow = const Color(0xffF9B621);
  static const Color brown = const Color(0xffc97417);
  static const Color brownLight = const Color(0xfff1c682);
//  static const Color newGray = const Color(0xffE4E4E4);
  static const Color newGray = const Color(0xffededed);

  //STEFANO COLORS

  // Grays
  static const Color textGray = const Color.fromRGBO(116, 122, 142, 1.0);
  static const Color textLightGray = const Color.fromRGBO(137, 167, 182, 1.0);
  static const Color textLighterGray = const Color.fromRGBO(164, 170, 188, 1.0);
  static const Color textVeryLightGray = const Color.fromRGBO(176, 197, 208, 1.0);


  // Gradient
  static const Color gradientStartOrange = const Color.fromRGBO(255, 106, 0, 1.0);
  static const Color gradientEndPink = const Color.fromRGBO(239, 15, 114, 1.0);


  // Shimmer
  static const Color shimmerBase = const Color.fromRGBO(215, 215, 215, 1.0);
  static const Color shimmerHighlight = const Color.fromRGBO(240, 240, 240, 1.0);

// Misc


}