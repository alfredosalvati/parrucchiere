import 'dart:async';
import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:parrucchiere/objects/media_content.dart';

import 'db/firestore_db.dart';
import 'home.dart';
import 'objects/services.dart';
import 'objects/who_content.dart';



class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}


class _SplashScreenState extends State<SplashScreen> {


  @override
  void initState() {
    super.initState();
    signInAnon().then((FirebaseUser user){
      print('Login success!');
      print('UID: ' + user.uid);
      _db = Firestore.instance;

      buildProductList();
      buildContent();
    });

//    initailiseDb();

    Timer(Duration(microseconds: 1000), () {
      setState(() {
        opacity = 1.0;
      });

    });


    Timer(
        Duration(seconds: 2),
            () =>
            Navigator.of(context).push(
                PageRouteBuilder(
                  pageBuilder: (context, animation1, animation2) {
                    return Home();
                  },
                  transitionsBuilder: (context, animation1, animation2, child) {
                    return FadeTransition(
                      opacity: animation1,
                      child: child,
                    );
                  },
                  transitionDuration: Duration(milliseconds: 1000),
                )));
  }

  double opacity = 0.0;
//  SessionBloc session;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    constraints: BoxConstraints(
                      maxHeight: 300,
                      minHeight: 300
                    ),
                    padding: EdgeInsets.only(bottom: 30),
                    child: Image.asset('assets/img/logo.png',
                      width: MediaQuery
                          .of(context)
                          .size
                          .width * 0.55,
                      //height: MediaQuery.of(context).size.height*0.4,
                    ),
                  ),
                ],
              )
          ),
        )
    );
  }

  List<Service> productList = new List();

  Firestore _db = Firestore.instance;
  FirestoreDb firestoreDb = new FirestoreDb();
  FirebaseAuth firebaseAuth = FirebaseAuth.instance;

  Future<FirebaseUser> signInAnon() async {
    AuthResult result = await firebaseAuth.signInAnonymously();
    FirebaseUser user = result.user;
    print("Signed in: ${user.uid}");
    return user;
  }


  void buildProductList() async {
    firestoreDb.getServices().then((value) => Service.services = value);
  }



  void buildContent() async {
    firestoreDb.getText().then((value) => WhoContent.content = value);
    firestoreDb.getWhoImages().then((value) => WhoContent.images = value);
    firestoreDb.getMediaImages().then((value) => MediaContent.images = value);
    firestoreDb.getMediaVideos().then((value) => MediaContent.videos = value);
  }

  //Initialise the db with products -- BE CAREFUL
  void initailiseDb() {
    print("Initialise");

    String dbString = '''
{
  "services": [
    {
      "id": "A",
      "image": [
        "https://firebasestorage.googleapis.com/v0/b/parrucchiere-dc956.appspot.com/o/parr1.jpg?alt=media&token=1db22bd0-7878-47d0-aab8-7f407a1c6e21"
      ],
      "name": "Taglio",
      "price": 1000,
      "description": "Taglio per uomo"
    },
    {
      "id": "B",
      "image": [
        "https://firebasestorage.googleapis.com/v0/b/parrucchiere-dc956.appspot.com/o/parr1.jpg?alt=media&token=1db22bd0-7878-47d0-aab8-7f407a1c6e21"
      ],
      "name": "Messa in piega",
      "price": 2000,
      "description": "Messa in piega per donna"
    },
    {
      "id": "C",
      "image": [
        "https://firebasestorage.googleapis.com/v0/b/parrucchiere-dc956.appspot.com/o/parr1.jpg?alt=media&token=1db22bd0-7878-47d0-aab8-7f407a1c6e21"
      ],
      "name": "Tinta ",
      "price": 3500,
      "description": "Tinta\\nPer uomo o donna."
    },
    {
      "id": "D",
      "image": [
        "https://firebasestorage.googleapis.com/v0/b/parrucchiere-dc956.appspot.com/o/parr1.jpg?alt=media&token=1db22bd0-7878-47d0-aab8-7f407a1c6e21"
      ],
      "name": "Barba",
      "price": 1000,
      "description": "Barba per donna"
    },
    {
      "id": "E",
      "image": [
        "https://firebasestorage.googleapis.com/v0/b/parrucchiere-dc956.appspot.com/o/parr1.jpg?alt=media&token=1db22bd0-7878-47d0-aab8-7f407a1c6e21"
      ],
      "name": "Shampoo",
      "price": 500,
      "description": "Shampoo per donna"
    },
    {
      "id": "F",
      "image": [
        "https://firebasestorage.googleapis.com/v0/b/parrucchiere-dc956.appspot.com/o/parr1.jpg?alt=media&token=1db22bd0-7878-47d0-aab8-7f407a1c6e21"
      ],
      "name": "Unghie",
      "price": 1000,
      "description": "Unghie con french"
    },
    {
      "id": "G",
      "image": [
        "https://firebasestorage.googleapis.com/v0/b/parrucchiere-dc956.appspot.com/o/parr1.jpg?alt=media&token=1db22bd0-7878-47d0-aab8-7f407a1c6e21"
      ],
      "name": "Colpi di sole",
      "price": 3000,
      "description": "Unghie con french"
    },
    {
      "id": "H",
      "image": [
        "https://firebasestorage.googleapis.com/v0/b/parrucchiere-dc956.appspot.com/o/parr1.jpg?alt=media&token=1db22bd0-7878-47d0-aab8-7f407a1c6e21"
      ],
      "name": "Permanente",
      "price": 3000,
      "description": "Unghie con french"
    }
  ]
}
    ''';

    Db db = new Db.fromJson(jsonDecode(dbString));

    print(db.services.length.toString());

    for (Service service in db.services) {
      _db.collection("menu").document("first")
          .collection("services")
          .document(service.id)
          .setData(service.toJson());
    }
  }
}
