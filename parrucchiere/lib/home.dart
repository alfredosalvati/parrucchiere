import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:parrucchiere/pages/contact.dart';
import 'package:parrucchiere/pages/media.dart';
//import 'package:parrucchiere/pages/media_video.dart';
import 'package:parrucchiere/pages/services.dart';
import 'package:parrucchiere/pages/who.dart';
import 'package:parrucchiere/util/contact_utils.dart';

import 'common/colors.dart';


class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<Home> with TickerProviderStateMixin {


  AnimationController _controller;
  PageController _myPage = PageController(initialPage: 0);

  static const List<IconData> icons = const [ Icons.sms, Icons.mail, Icons.phone ];

  @override
  void initState() {
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
  }

  var height2 = 5.0;

  @override
  Widget build(BuildContext context) {
    Color backgroundColor = Theme.of(context).cardColor;
    Color foregroundColor = Theme.of(context).accentColor;

    return Scaffold(
      body: PageView(
        physics:new NeverScrollableScrollPhysics(),
        controller: _myPage,
        onPageChanged: (int) {
          print('Page Changes to index $int');
        },
        children: <Widget>[
         Who(AppColors.brown),
          ServiceList(AppColors.yellow),
          Media(),
          ContactPage()
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        child: Container(
          height: 75,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              IconButton(
                iconSize: 30.0,
                padding: EdgeInsets.only(left: 28.0),
                icon: Icon(Icons.home),
                onPressed: () {
                  setState(() {
                    _myPage.animateToPage(0, duration: Duration( milliseconds: 200), curve: Curves.linear);

                  });
                },
              ),
              IconButton(
                iconSize: 30.0,
                padding: EdgeInsets.only(right: 28.0),
                icon: Icon(Icons.room_service),
                onPressed: () {
                  setState(() {
                    _myPage.animateToPage(1, duration: Duration( milliseconds: 200), curve: Curves.linear);
                  });
                },
              ),
              IconButton(
                iconSize: 30.0,
                padding: EdgeInsets.only(left: 28.0),
                icon: Icon(Icons.video_library),
                onPressed: () {
                  setState(() {
                    _myPage.animateToPage(2, duration: Duration( milliseconds: 200), curve: Curves.linear);
                  });
                },
              ),
              IconButton(
                iconSize: 30.0,
                padding: EdgeInsets.only(right: 28.0),
                icon: Icon(Icons.list),
                onPressed: () {
                  setState(() {
                    _myPage.animateToPage(3, duration: Duration( milliseconds: 200), curve: Curves.linear);
                  });
                },
              )
            ],
          ),
        ),
      ),
      floatingActionButton: new Column(
      mainAxisSize: MainAxisSize.min,
//      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: new List.generate(icons.length, (int index) {
        Widget child = new AnimatedContainer(
          duration: Duration(milliseconds: 500),
          color: Colors.transparent,
          height: height2,
          width: 56.0,
          alignment: FractionalOffset.topCenter,
          child: new ScaleTransition(
            scale: new CurvedAnimation(
              parent: _controller,
              curve: new Interval(
                  0.0,
                  1.0 - index / icons.length / 2.0,
                  curve: Curves.easeOut
              ),
            ),
            child: new FloatingActionButton(
              heroTag: null,
              backgroundColor: backgroundColor,
              mini: true,
              child: new Icon(icons[index], color: foregroundColor),
              onPressed: () {
                if(index == 0 ) ContactUtils.launchWhatsApp(phone: "21213123123", message: "Ciao");
                else if (index == 1) ContactUtils.email("info@parrucchiere.com");
                else if (index == 2) ContactUtils.call("21213123123");
              },
            ),
          ),
        );
        return child;
      }).toList()..add(
        new FloatingActionButton(
          heroTag: null,
          child: new AnimatedBuilder(
            animation: _controller,
            builder: (BuildContext context, Widget child) {
              return new Transform(
                transform: new Matrix4.rotationZ(_controller.value * 0.5 * math.pi),
                alignment: FractionalOffset.center,
                child: new Icon(_controller.isDismissed ? Icons.share : Icons.close),
              );
            },
          ),
          onPressed: () {
            if (_controller.isDismissed) {
              print("up");
              setState(() {
                height2 = 60;
              });
              _controller.forward();
            } else {
              print("down");
              setState(() {
                height2 = 5;
              });
              _controller.reverse();
            }
          },
        ),
      ),
    ),
    floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked

    );
  }

}